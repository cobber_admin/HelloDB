package site.zhanjingbo.model;

/**
 * SQL注入演示DEMO的实体类
 * @author zhanjingbo
 *
 */

public class User {
	private int id;
	private String username;
	private String password;
	private boolean sex;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isSex() {
		return sex;
	}

	public void setSex(boolean sex) {
		this.sex = sex;
	}

	@Override
	public String toString() {
		return "用户 [id=" + id + ", username=" + username + ", password=" + password + ", sex=" + sex + "]";
	}
	
}
