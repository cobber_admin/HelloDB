package site.zhanjingbo.HelloDB;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.commons.dbcp2.BasicDataSource;

public class HelloDBCP {

	static final String DRIVER_NAME = "com.mysql.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://localhost:3307/cloud_study?characterEncoding=utf8";
	static final String DB_USER = "root";
	static final String DB_PASSWORD = "";

	public static BasicDataSource ds = null;

	public static void init() {
		ds = new BasicDataSource();
		ds.setDriverClassName(DRIVER_NAME);
		ds.setUsername(DB_USER);
		ds.setUrl(DB_URL);
		ds.setPassword(DB_PASSWORD);
	}

	public static void search() {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery("select username from user");
			while (rs.next()) {
				System.out.println("Hello " + rs.getString("username"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
				if (stmt != null) {
					stmt.close();
				}
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
			}
		}
	}

	public static void main(String[] args) {
		init();
		search();
	}
}
