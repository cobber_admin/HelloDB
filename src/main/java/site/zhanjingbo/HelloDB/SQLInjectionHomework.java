package site.zhanjingbo.HelloDB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SQLInjectionHomework {
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://localhost:3307/cloud_study";
	static final String USER = "root";
	static final String PASS = "";

	public static void getStudent(String name) throws ClassNotFoundException {
		Connection conn = null;
		PreparedStatement ptmt = null;
		ResultSet rs = null;
		try {
			Class.forName(JDBC_DRIVER);
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			ptmt = conn.prepareStatement("select name,number from student where name=?");
			ptmt.setString(1, name);
			rs = ptmt.executeQuery();
			while (rs.next()) {
				System.out.println(rs.getString("name") + ":" + rs.getInt("number"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception e) {
					// ignore
				}
			}
			if (ptmt != null) {
				try {
					ptmt.close();
				} catch (Exception e) {
					// ignore
				}
			}
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// ignore
				}
			}
		}
	}

	public static void main(String[] args) {
		try {
			getStudent("XiaoMing");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
}
